//
//  Events.swift
//  teste
//
//  Created by Gustavo Cosme on 20/08/20.
//  Copyright © 2020 Gustavo Cosme. All rights reserved.
//

import NotificationCenter;

extension Notification.Name
{
    
    static let onFavorite = Notification.Name("onFavorite");
    static let onDesfavorite = Notification.Name("onDesfavorite");
    static let onThemeChange = Notification.Name("onThemeChange");

}
