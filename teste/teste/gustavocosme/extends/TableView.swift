//
//  TableView.swift
//  teste
//
//  Created by Gustavo Cosme on 19/08/20.
//  Copyright © 2020 Gustavo Cosme. All rights reserved.
//

import UIKit

extension UITableView
{
    
    public func registerCell(cell:String)
    {
        self.register(UINib(nibName: cell, bundle: nil), forCellReuseIdentifier: cell)
    }
    
    
    
    
    
    
}
