//
//  UIViewController.swift
//  teste
//
//  Created by Gustavo Cosme on 21/08/20.
//  Copyright © 2020 Gustavo Cosme. All rights reserved.
//

import UIKit

class UIViewControllerGC:UIViewController  {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        onUpdateTheme()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onChangeTheme(_:)), name: .onThemeChange, object:nil);
        
    }
    
    @objc func onChangeTheme(_ notification: Notification)
    {
        onUpdateTheme()
    }
    
    func onUpdateTheme()
    {
        if(AppDelegate.APP.idTema == 0)
        {
            overrideUserInterfaceStyle = .light
        }
        else
        if(AppDelegate.APP.idTema == 1)
        {
            overrideUserInterfaceStyle = .dark
        }
        else{
            
            overrideUserInterfaceStyle = .unspecified;

        }
    }
}

extension UIViewController
{
    
    
    func setTitleInterna(value:String){
        
        self.navigationItem.title = value;
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = #colorLiteral(red: 0.9331802726, green: 0.3883583546, blue: 0.2146830857, alpha: 1);
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationItem.rightBarButtonItems = [];
        
    }
    
    /*
     func addMenuSlide()
     {
     
     let button:UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_menu_32"), style:.plain, target: nil, action:#selector(onSelectMenu));
     navigationItem.leftBarButtonItems = [button];
     
     
     }
     */
    
    
    
    /*
     
     func delay(_ delay: Double, closure:@escaping () -> Void) {
     DispatchQueue.main.asyncAfter(
     deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
     }
     
     */
    
    func hideKeyboardWhenTappedAround()
    {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboardView")
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
    }
    
    func dismissKeyboardView() {
        view.endEditing(true)
    }
    
    func addAlertOK(value:String)
    {
        let alertController = UIAlertController(title:nil, message: value, preferredStyle: .alert)
        let OKAction = UIAlertAction(title:"OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    
    
    
}//END CLASS
