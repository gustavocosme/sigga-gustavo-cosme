//
//  Nav.swift
//  teste
//
//  Created by Gustavo Cosme on 23/08/20.
//  Copyright © 2020 Gustavo Cosme. All rights reserved.
//

import UIKit

class Nav: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

       
        onUpdateTheme()

        NotificationCenter.default.addObserver(self, selector: #selector(onChangeTheme(_:)), name: .onThemeChange, object:nil);
                
    }
            
    @objc func onChangeTheme(_ notification: Notification)
    {
        onUpdateTheme()
    }
    
    func onUpdateTheme()
    {
       if(AppDelegate.APP.idTema == 0)
       {
           overrideUserInterfaceStyle = .light
       }
       else
       if(AppDelegate.APP.idTema == 1)
       {
           overrideUserInterfaceStyle = .dark
       }else{
           
           overrideUserInterfaceStyle = .unspecified;

       }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
