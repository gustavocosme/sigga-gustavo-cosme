//
//  HTTP.swift
//  casacorpe
//
//  Created by Safari Mobile on 24/08/2018.
//  Copyright © 2018 Safari Mobile. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PKHUD

class DataUploadImage: NSObject {
    
    var image:UIImage!;
    var nameParams  = "";

}


enum ROUTES: String {
    
    case POSTS
    
    var name: String {
        get {
            
            switch self
            {
            case .POSTS:
                return "\(API.SERVER)posts?_page=1&_limit=70"
                
            }
            
        }
    }
}


class API: NSObject {
    
    
    public static var IS_DEBUG                                 = false;
    public static var SERVER_RELEASE                           = "http://jsonplaceholder.typicode.com/";
    public static var SERVER_DEBUG                             = "http://jsonplaceholder.typicode.com/";
    private static var ERRO                                    = "Ops estamos melhorando nossos serviços";
    static var SERVER                                          = API.IS_DEBUG == true ?API.SERVER_DEBUG:API.SERVER_RELEASE;
    
    
    static func request(URL:ROUTES,completion: @escaping (JSON,String)->Void)
    {
        
        URLCache.shared.removeAllCachedResponses()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true;
        
        AF.request("\(URL.name)",method:.get).responseJSON { response in
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false;
            
            switch response.result {
                
            case .success:
                
                do {
                    
                    let json = try JSON(data: response.data!);
                    completion(json,"")
                    
                } catch {
                    
                    completion(JSON(),error.localizedDescription);
                    
                }
                
            case .failure(let error):
                
                completion(JSON(),error.localizedDescription);
                
                
                
            }
        }
        
        
    }
    

    static func upload(_ url: String, images: [DataUploadImage], params: [String : Any],completion: @escaping ((JSON) -> Void)) {
           
        
        
        
           URLCache.shared.removeAllCachedResponses()
           UIApplication.shared.isNetworkActivityIndicatorVisible = true;
           
           HUD.dimsBackground              = true;
           HUD.allowsInteraction           = false;
                   
           if let topVC = UIApplication.getTopViewController() {
                    
               HUD.show(.progress, onView: topVC.view);
                       
           }
           
           let httpHeaders = HTTPHeaders(["Content-Type":"application/json"])
        
           AF.upload(multipartFormData: { multiPart in
            
               for p in params {
                   multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
               }
            
                for i in images {
                    
                    let identifier = UUID()
                    multiPart.append(i.image.jpegData(compressionQuality: 0.3)!, withName: "\(i.nameParams)", fileName: "\(identifier.uuidString).jpg", mimeType: "image/jpg")
                    
                }
               
           }, to: url, method: .post, headers: httpHeaders) .uploadProgress(queue: .main, closure: { progress in
               print("Upload Progress: \(progress.fractionCompleted)")
           }).responseJSON(completionHandler: { data in
               
               print("upload finished: \(data)")
           }).response { (response) in
               
               UIApplication.shared.isNetworkActivityIndicatorVisible = false;
               HUD.hide();

               do {
                   
                   let json = try JSON(data: response.data!);
                   print(json.description);
                
                    DispatchQueue.global().async {
                        completion(json);
                    }
                   
                   
               }catch {
                   
                    if let topVC = UIApplication.getTopViewController() {
                                      
                       topVC.addAlertOK(value: "Estamos melhorando nossos serviços");

                    }
                    
                    print(error.localizedDescription);
                   
               }
               
           }
       }
    
    

    
    
    

    
    
    
}//END CLASS
