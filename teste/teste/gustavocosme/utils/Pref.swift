//
//  Pref.swift
//  modeloios
//
//  Created by Gustavo Cosme on 29/11/16.
//  Copyright © 2016 Safari Studio. All rights reserved.
//

import Foundation



class Pref:NSObject {
 
    static let user = UserDefaults.standard
    
    static func add(params:Any,key:String)
    {
        user.setValue(params, forKey:key);
        user.synchronize();
    }
    
    static func remove(key:String)
    {
        user.removeObject(forKey:key);
        user.synchronize();
    }
    
    static func get(key:String)->Any
    {
        
        
        if(user.object(forKey:key) as? NSObject != nil)
        {
            return user.object(forKey:key) as! NSObject;

        }
        
        return -1;
    }
    
}//END CLASS

    
