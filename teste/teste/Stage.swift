//
//  Stage.swift
//  opovoalerta
//
//  Created by Gustavo Cosme on 17/07/17.
//  Copyright © 2017 Guga. All rights reserved.
//

import UIKit
import Foundation

class Stage: NSObject {
    
    var storyboardMain                                  :UIStoryboard!
    
    override init()
    {
        storyboardMain      = UIStoryboard(name: "Main", bundle: nil)
    }
    
    func getPos() -> Post
    {
        return storyboardMain.instantiateViewController(withIdentifier:"Post") as! Post;
    }
    
    func getSobre() -> UIViewController
    {
        return storyboardMain.instantiateViewController(withIdentifier:"Sobre") as! UIViewController;
    }
    
    func getAjuda() -> Ajuda
    {
        return storyboardMain.instantiateViewController(withIdentifier:"Ajuda") as! Ajuda;
    }
    
   
}//END CLASS


