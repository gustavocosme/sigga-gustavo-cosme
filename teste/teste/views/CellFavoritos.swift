//
//  CellPosts.swift
//  teste
//
//  Created by Gustavo Cosme on 19/08/20.
//  Copyright © 2020 Gustavo Cosme. All rights reserved.
//

import UIKit
import Kingfisher;
import MGSwipeTableCell

class CellFavoritos: MGSwipeTableCell {

    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var foto: UIImageView!
    var params:DataPost!;
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
 
    
    func inicialize(params:DataPost)
    {
       
        self.params = params;
        titulo.text                     = params.title;
        let url = URL(string:"http://gustavocosme.com/test/imgs/f1.png")
        foto.kf.setImage(with: url);
        
       let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
       self.contentView.addGestureRecognizer(gesture)
        
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {

        if let top = UIApplication.getTopViewController() {
                        
                   top.view.endEditing(false);
                   let v = AppDelegate.STAGE.getPos();
                   v.data = params;
                   top.navigationController?.pushViewController(v, animated: true);
                   
        }
        
    }
 
    
    
    
}
