//
//  CellPosts.swift
//  teste
//
//  Created by Gustavo Cosme on 19/08/20.
//  Copyright © 2020 Gustavo Cosme. All rights reserved.
//

import UIKit
import Kingfisher;

class CellPosts: UITableViewCell {

    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var foto: UIImageView!
    @IBOutlet weak var btn: UIButton!
    var params:DataPost!;
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
 
    
    func inicialize(params:DataPost)
    {
       
        self.params = params;
        titulo.text                     = params.title.uppercased();
        
        
        let url = URL(string:"http://gustavocosme.com/test/imgs/f1.png")
        
        let processor = DownsamplingImageProcessor(size: foto.bounds.size)
        >> RoundCornerImageProcessor(cornerRadius: 0)
        foto.kf.setImage(with: url,options: [
            .processor(processor),
            .scaleFactor(UIScreen.main.scale),
            .transition(.fade(1)),
            .cacheOriginalImage
        ]);
        
        
        if FavoriteModel.isFavorite(id: params.id)
        {
            
            btn.setImage(UIImage(named: "ic_favotitar"), for: .normal);
            
        }else{

            btn.setImage(UIImage(named: "ic_desfavoritar"), for: .normal);

        }
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        self.contentView.addGestureRecognizer(gesture)
        
        
    }
    
    @IBAction func onClickFavoritar(_ e:Any)
    {
        if FavoriteModel.isFavorite(id: params.id)
        {
                      
            FavoriteModel.onDesfavorite(id:(params.id))

        }else{
                        
            FavoriteModel.onFavorite(value: params);
            
        }
        
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {

        if let top = UIApplication.getTopViewController() {
                        
                   top.view.endEditing(false);
                   let v = AppDelegate.STAGE.getPos();
                   v.data = params;
                   top.navigationController?.pushViewController(v, animated: true);
                   
        }
        
    }

    

    
    
}
