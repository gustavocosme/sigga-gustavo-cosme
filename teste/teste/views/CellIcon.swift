//
//  ProgramacaoCell.swift
//  PEnoCarnaval
//
//  Created by Gustavo Cosme on 31/08/18.
//  Copyright © 2018 SafariStudio. All rights reserved.
//

import UIKit;
import  SwiftyJSON;


class CellIcon: UITableViewCell {
    
    
    @IBOutlet weak var txtTitulo        :UILabel!;
    @IBOutlet weak var icon             :UIImageView!;
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
        
    }
    
    func initJson(data:NSDictionary)
    {
        txtTitulo.text = data.value(forKey: "title") as! String
        icon.image = data.value(forKey: "icon") as! UIImage;
        
        
    }
    
    
    
    
}//END CLASS
