//
//  HomeModel.swift
//  teste
//
//  Created by Gustavo Cosme on 19/08/20.
//  Copyright © 2020 Gustavo Cosme. All rights reserved.
//

import UIKit
import SwiftyJSON

class PostsModel: NSObject {
    
    
    
    
    static func connect(completion: @escaping ()->Void)
    {
    
        completion();
        
        API.request(URL:.POSTS,completion:{json,status in
    
            print(json.description);
            
            popule(value: json.array ?? [JSON](),completion: completion);


        });
    
    }
    
    private static func popule(value:[JSON],completion: @escaping ()->Void)
    {
        
        if(value.count > 0)
        {
            deleteAll();
        }
        
        for i:Int in 0 ..< value.count
        {
            
            let json         = value[i];
            let db           = DataPost();
            
            if(json["title"] != JSON.null)
            {
                db.title      = json["title"].string!;
                
            }else{
                
                db.title           = "";
                
            }
            
            if(json["id"] != JSON.null)
            {
                db.id      = json["id"].int!;
                
            }else{
                
                db.id           = -1;
                
            }
            
            if(json["body"] != JSON.null)
            {
                db.body      = json["body"].string!;

            }else{
                
                db.body      = "";

            }
            
            if(json["userId"] != JSON.null)
            {
                db.id      = json["userId"].int!;
                
            }else{
                
                db.id           = -1;
                
            }
            
            db.save();
            
        }
        
        completion();
        
        
    }
    
    private static func deleteAll()
    {
        AppDelegate.db.execute(sql: "DELETE FROM \(DataPost.NAME)");
    }
    
    static func getAll()->[DataPost]
    {
        var resp = [DataPost]();
        
        for i in AppDelegate.db.query(sql: "SELECT * FROM \(DataPost.NAME)")
        {
            let d = DataPost();
            d.body = i["body"] as! String;
            d.title = i["title"] as! String;
            d.id = i["id"] as! Int;
            d.userId = i["userId"] as! Int;
            resp.append(d);

        }
        
        return resp
    }
    
    static func getPesquisaNomeAll(searchText:String)->[DataPost]
    {
        var resp = [DataPost]();

        for i in AppDelegate.db.query(sql:"SELECT * FROM \(DataPost.NAME) WHERE title LIKE '%\(searchText)%' OR body LIKE '%\(searchText)%'")
             {
                 let d = DataPost();
                 d.body = i["body"] as! String;
                 d.title = i["title"] as! String;
                 d.id = i["id"] as! Int;
                 d.userId = i["userId"] as! Int;
                 resp.append(d);

             }
             
             return resp
    }
    
    
    
    
    
    
    
}
