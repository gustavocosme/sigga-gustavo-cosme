//
//  ModelFavorite.swift
//  vamoz
//
//  Created by Gustavo Cosme on 26/08/19.
//  Copyright © 2019 Gustavo Cosme. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class FavoriteModel: NSObject {
    
    
    static func isFavorite(id:Int)->Bool {
        
        
        let appDelegate                 = UIApplication.shared.delegate as! AppDelegate
        let context                     = appDelegate.persistentContainer.viewContext
        let request                     = NSFetchRequest<NSFetchRequestResult>(entityName: "DataFavorito")
        request.predicate               = NSPredicate(format: "id == \(id)")
        request.returnsObjectsAsFaults  = false
        
        do {
            
            let result = try context.fetch(request)

            if(result.count == 0)
            {
                
                return false;
            
            }
            else{
                
                return true;
                
            }
           
            
            
        } catch {
            
            return false;
            
        }
        
    }
    
    static func getAll()->[DataPost]
    {
        
        var dados = [DataPost]();
        let appDelegate                 = UIApplication.shared.delegate as! AppDelegate
        let context                     = appDelegate.persistentContainer.viewContext
        let request                     = NSFetchRequest<NSFetchRequestResult>(entityName: "DataFavorito")
        //request.predicate               = NSPredicate(format: "quantidade > 0")
        request.returnsObjectsAsFaults  = false
        
        do {
            
            let result = try context.fetch(request)
            
            for data in result as! [DataFavorito] {
                
                let d = DataPost();
                d.body = data.body!;
                d.id = Int(data.id);
                d.title = data.title!;
                d.userId = Int(data.userId);
                
                dados.append(d);
            }
            
            return dados.reversed();
            
        } catch {
            
            return [DataPost]();
        }
        
    }
    
    
    static func getCount()->Int
    {
        let appDelegate                 = UIApplication.shared.delegate as! AppDelegate
        let context                     = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "DataFavorito")
        let count = try! context.count(for: fetchRequest)
        return count
        
    }
    
    
    static func onFavorite(value:DataPost)->Bool
    {
        
        
        
        let appDelegate     = UIApplication.shared.delegate as! AppDelegate
        let context         = appDelegate.persistentContainer.viewContext
        
        let data:DataFavorito = DataFavorito(context: context);
        data.id               = Int16(value.id);
        data.userId           = Int16(value.userId);
        data.body             = value.body;
        data.title            = value.title;

        do{
            
            try context.save();
            var dataParams:[String:Any] = [String:Any]();
            dataParams["data"] = data
            NotificationCenter.default.post(name: .onFavorite, object: dataParams);
            print(data.description);
            
            return true;
            
        } catch {
            
            return false;
            
        }
        
        
    }
    
    
    static func onDesfavorite(id:Int)
    {
        
        let appDelegate                 = UIApplication.shared.delegate as! AppDelegate
        let context                     = appDelegate.persistentContainer.viewContext
        let request                     = NSFetchRequest<NSFetchRequestResult>(entityName: "DataFavorito")
        request.predicate               = NSPredicate(format: "id == \(id)")
        request.returnsObjectsAsFaults  = false
        
        do{
            
            let result = try context.fetch(request)
            for data in result as! [DataFavorito] {
                
                context.delete(data);
            }
            
            do{
            
                try context.save();
                NotificationCenter.default.post(name: .onDesfavorite, object: nil)
                
            } catch{
                
                
            }
            
            
        } catch {
            
        }
        
    }
    
    
    
    
    

    
}//END CLASS
