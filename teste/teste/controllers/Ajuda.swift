//
//  UploadDocViewController.swift
//  federalpetroleo
//
//  Created by Gustavo Cosme on 13/08/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
import SwiftyJSON;

class Ajuda: UIViewControllerGC, UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {
    
    
    @IBOutlet weak var txt             : UITextField!
    @IBOutlet weak var image           : UIImageView!
    
    let imagePicker  = UIImagePickerController()
    var dataImageUIImage1               :UIImage!;
    
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        
        txt.delegate = self;
        imagePicker.delegate = self;
        
        image.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(Ajuda.onPhoto))
        image.addGestureRecognizer(tap)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        onClick(sender: textField);
        
        return true
    }
    
    @IBAction func onClick(sender: Any) {
        
        view.endEditing(true);
        
        if(txt.text!.count == 0)
        {
            addAlertOK(value: "Informe o problema!");
            return;
        }
        
        if(dataImageUIImage1 == nil)
        {
            addAlertOK(value: "Adicione a imagem");
            return;
        }
        
        var p:[String:Any] = [String:Any]();
        p["profile_id"] = txt.text!
        
        let i1 = DataUploadImage();
        i1.nameParams = "imagem";
        i1.image = dataImageUIImage1;
        
        API.upload("http://mundosafari.com.br/mobile/test/upload.php", images:[i1], params: p, completion: {json in
                   
            DispatchQueue.main.async {
                
                print(json.description);
                
                let actionSheetController: UIAlertController = UIAlertController(title:nil, message: "Ajuda enviada com sucesso!", preferredStyle: .alert)
                       
                       
                       
                       let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
                           
                           
                            self.dismiss(animated: true, completion: nil);
                           
                           
                       }
                       
                       actionSheetController.addAction(okAction)
                       self.present(actionSheetController, animated: true, completion: nil)
                       
                

            }
            
        })
        
        

        
        
        
    }
    
    @IBAction func onClickClose(sender: Any) {
        
        dismiss(animated: true, completion: nil);
        
    }
  
    
    //###################################################//
    //MARK:PHOTO
    //###################################################//
    
    @IBAction func onClickDelete(sender: Any) {
        
        image.image = #imageLiteral(resourceName: "ic_form_big_foto");
        
    }
    
    
    @objc func onPhoto() {
        
        
        
        
        let refreshAlert = UIAlertController(title: "Foto perfil", message: "Modo de carregamento.", preferredStyle:.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Biblioteca", style: .default, handler: { (action: UIAlertAction!) in
            
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Foto", style: .default, handler: { (action: UIAlertAction!) in
            
            if !UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
                
                return
            }
            
            self.imagePicker.allowsEditing = false;
            self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.imagePicker.cameraCaptureMode = .photo
            
            //self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(self.imagePicker,
                         animated: true,
                         completion: nil)
            
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
            
            
        }))
        
        present(refreshAlert, animated: true, completion: nil);
        
        
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        
        var newImage: UIImage
        
        if let possibleImage = info[.editedImage] as? UIImage {
            
            newImage = possibleImage
            self.dataImageUIImage1 = newImage.normalizedImage();
            self.image.image = dataImageUIImage1
            
        } else if let possibleImage = info[.originalImage] as? UIImage {
            
            newImage = possibleImage
            self.dataImageUIImage1 = newImage.normalizedImage();
            
            self.image.image = dataImageUIImage1
            
        } else {
            return
        }
        
        
        dismiss(animated: true)
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    
    
    
    
    
    
}//END CLASS
