//
//  ProgramacaoInterna.swift
//  agendarecife
//
//  Created by Gustavo Cosme on 14/05/20.
//  Copyright © 2020 Safari. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
import SafariServices

class Post: UIViewController {

    @IBOutlet weak var txtTitulo: UILabel!
    @IBOutlet weak var txtDescricao: UILabel!
    @IBOutlet weak var foto:UIImageView!

    var btnFavorito:UIBarButtonItem!
    var btnShared:UIBarButtonItem!

    var data:DataPost!;

    override func viewDidLoad() {
    
        super.viewDidLoad();
        
        setTitleInterna(value: "Post")
        
        NotificationCenter.default.addObserver(self, selector: #selector(onChangeFavoriteEvent(_:)), name: .onFavorite, object:nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onChangeFavoriteEvent(_:)), name: .onDesfavorite, object:nil);
        
        
        DispatchQueue.global(qos: .utility).async {

                      DispatchQueue.main.async {
                       
                        self.set();


                      }
                  }
        
        
    }
    
    //MARK: SET
    
    func set()
    {
        
        
        txtTitulo.text     = data.title.uppercased();
        txtDescricao.text  = data.body;

        let imagem = "http://gustavocosme.com/test/imgs/f1.png"
        let url = URL(string:imagem)
        foto.kf.setImage(with: url);

        btnShared = UIBarButtonItem(image: UIImage(named: "ic_share"), style: .plain, target: self, action: #selector(onClickShared))
        btnShared.tintColor = #colorLiteral(red: 0.9331802726, green: 0.3883583546, blue: 0.2146830857, alpha: 1);
        
        btnFavorito = UIBarButtonItem(image:UIImage(named: "ic_heart_24"), style: .plain, target: self, action: #selector(onClickFavorite))
        btnFavorito.tintColor = #colorLiteral(red: 0.9331802726, green: 0.3883583546, blue: 0.2146830857, alpha: 1);
        
        
        navigationItem.rightBarButtonItems = [btnShared,btnFavorito];
        
        onChangeFavorite();
        
        
    }
    
    //MARK: EVENTS

    @objc func onChangeFavoriteEvent(_ notification: Notification)
    {
        onChangeFavorite();
    }
    
    func onChangeFavorite()
    {
        if(FavoriteModel.isFavorite(id: data.id))
        {
            btnFavorito.image = UIImage(named: "ic_heart_fill_24");

            
        }else{
            
            btnFavorito.image = UIImage(named: "ic_heart_24");

        }
    }
    
    //MARK: CLICKs
    
    @objc func onClickFavorite()
    {
        if(FavoriteModel.isFavorite(id: data.id))
        {
            
            FavoriteModel.onDesfavorite(id: data.id);
            
        }else{
            
            FavoriteModel.onFavorite(value: data);
            
        }
        
        onChangeFavorite();
    }
    
    @objc func onClickShared()
    {
          let a:String = data.title+" - "+data.body;
          
          let messageStr:String  = "\(a)"
          
          let shareItems:Array = [messageStr]
          
          let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
          activityViewController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
          
          self.present(activityViewController, animated: true, completion: nil);
    }
    


}//END CLASS
