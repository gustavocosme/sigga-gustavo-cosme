//
//  Pets.swift
//  zoapet
//
//  Created by Gustavo Cosme on 05/02/19.
//  Copyright © 2019 SafariStudio. All rights reserved.
//

import UIKit
import SwiftyJSON;
import MGSwipeTableCell

class Favoritos: UIViewController, UITableViewDelegate ,UITableViewDataSource {
    
    @IBOutlet weak var aviso: UIView!
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl();
    var dados = [DataPost]();
    
    @IBOutlet weak var btnEditar: UIBarButtonItem!
    var isEdit = false;
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        inicialize();
        
        self.aviso.alpha = 0.0
        UIView.animate(withDuration: 1.5) {
               self.aviso.alpha = 1.0
              
        }
        
        
    }
    
    func register()
    {
        tableView.registerCell(cell: "CellFavoritos");
        tableView.rowHeight = UITableView.automaticDimension;
        tableView.estimatedRowHeight = 77.0;
        tableView.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func inicialize(){
        
        
        register();
        
        self.onRender();
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.onRender();
        tableView.reloadData();
        
        
    }
    
    //MARK: TABLEVIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dados.count;
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 115;
        
    }
    
    var entrou = false;
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellFavoritos") as! CellFavoritos;
        cell.inicialize(params:  dados[indexPath.row]);
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
    
    func onRender(){
        
        self.dados = FavoriteModel.getAll();
        
        
        
        if(dados.count > 0)
        {
            aviso.isHidden = true;
            
        }else{
            
            
            aviso.isHidden = false;
            
        }
        
        tableView.reloadData();
        
    }
    
    @IBAction func onClickEdit(_ e:Any)
    {
        
        if(!isEdit)
        {
            
            btnEditar.title = "OK";
            isEdit = true;
            
            
            
        }else{
            
            btnEditar.title = "Editar";
            isEdit = false;
            
        }
        
        self.tableView.setEditing(self.isEdit, animated: true);

    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            FavoriteModel.onDesfavorite(id: Int(self.dados[indexPath.row].id));
            onRender();
        }
        
    }
    
    
    
   
    

    
    
    
}//END CLASS
