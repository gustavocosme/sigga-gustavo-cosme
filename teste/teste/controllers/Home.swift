//
//  Pets.swift
//  zoapet
//
//  Created by Gustavo Cosme on 05/02/19.
//  Copyright © 2019 SafariStudio. All rights reserved.
//

import UIKit
import SwiftyJSON;


class Home: UIViewControllerGC, UITableViewDelegate ,UITableViewDataSource {
    
    var dados                           :[DataPost] = [DataPost]();
    @IBOutlet weak var tableView: UITableView!
    var pull        :UIRefreshControl               = UIRefreshControl();

    override func viewDidLoad() {
        super.viewDidLoad();

        tableView.registerCell(cell: "CellPosts");
        
        pull.addTarget(self, action: #selector(onRefresc), for: .valueChanged)
        tableView.addSubview(pull);
        initModel();
        
        NotificationCenter.default.addObserver(self, selector: #selector(onChangeFavorite(_:)), name: .onFavorite, object:nil);
        NotificationCenter.default.addObserver(self, selector: #selector(onChangeFavorite(_:)), name: .onDesfavorite, object:nil);
            
    }
        
    @objc func onChangeFavorite(_ notification: Notification)
    {
        tableView.reloadData();
    }
    
    //MARK: TABLEVIEW
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dados.count;

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 493;

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellPosts") as! CellPosts;
        cell.inicialize(params: dados[indexPath.row]);
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
        
    }
    
  
    
    
    func initModel(){
  
        PostsModel.connect {
            
 
              self.dados = PostsModel.getAll();
              self.tableView.reloadData();
              self.pull.endRefreshing();


            
        }
        
        
    }
    
    @objc func onRefresc(refreshControl: UIRefreshControl) {
        

        initModel();
        
    }
    
    
}//END CLASS
