//
//  Pesquisa.swift
//  teste
//
//  Created by Gustavo Cosme on 20/08/20.
//  Copyright © 2020 Gustavo Cosme. All rights reserved.
//

import UIKit

class Pesquisa:Favoritos, UISearchBarDelegate
{
    
    @IBOutlet weak var pesquisa: UISearchBar!
    @IBOutlet weak var txtAviso: UILabel!
    
    override func viewDidLoad()
    {
        
        register();
        pesquisa.delegate = self;
        
        aviso.isHidden = false;
        
        self.aviso.alpha = 0.0
        UIView.animate(withDuration: 1.5) {
               self.aviso.alpha = 1.0
              
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    //###############################################################//
    //MARK:PESQUISA
    //###############################################################//
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        dados = PostsModel.getPesquisaNomeAll(searchText: searchText)
        
        if(searchText == "")
        {
            aviso.isHidden = false;
            dados.removeAll();
            
            txtAviso.text = "Não há resultado"
            
        }else{
            
            if(dados.count <= 0)
            {
                txtAviso.text = "Não há resultado: \(searchText)"
                aviso.isHidden = false;

                
            }else{
                
                aviso.isHidden = true;

            }
            
            
        }
        
        
        self.tableView.reloadData()
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        view.endEditing(true);
        
    }
    
    @IBAction func onClick(_ e:Any)
    {
        view.endEditing(true);
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        view.endEditing(true);

        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
    
    
}
