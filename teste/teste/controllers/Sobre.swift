//
//  Sobre.swift
//  teste
//
//  Created by Gustavo Cosme on 21/08/20.
//  Copyright © 2020 Gustavo Cosme. All rights reserved.
//

import UIKit

class Sobre: UIViewControllerGC {
    
    @IBOutlet weak var topo:UINavigationBar!

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(true)
         topo.shouldRemoveShadow(true);
     }
    
    @IBAction func onClick(_ e:Any)
    {
        dismiss(animated: true, completion: nil);
    }
    
    


}

extension UINavigationBar {
  
  func shouldRemoveShadow(_ value: Bool) -> Void {
      if value {
          self.setValue(true, forKey: "hidesShadow")
      } else {
          self.setValue(false, forKey: "hidesShadow")
      }
  }
}
