//
//  Programacao.swift
//  PEnoCarnaval
//
//  Created by Gustavo Cosme on 31/08/18.
//  Copyright © 2018 SafariStudio. All rights reserved.
//

import UIKit
import SwiftyJSON;

class Configuracoes:UIViewController, UITableViewDelegate ,UITableViewDataSource
{
    
    var dados       :[NSDictionary]             = [NSDictionary]();
    @IBOutlet weak var tableView: UITableView!
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    @IBOutlet weak var menu: UISegmentedControl!

    //MARK: INIT
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
        tableView.registerCell(cell: "CellIcon");

        dados.append(["title":"Sobre","icon":#imageLiteral(resourceName: "ic_sobre")]);
        dados.append(["title":"Precisando de ajuda?","icon":#imageLiteral(resourceName: "ic_ajuda_24")]);
        dados.append(["title":"Versão \(appVersion!)","icon":#imageLiteral(resourceName: "ic_dev")]);
        
        DispatchQueue.global(qos: .utility).async {
                   
                   DispatchQueue.main.async {
                    
                    print((Pref.get(key: "tema") as! Int));

                    self.menu.selectedSegmentIndex = (Pref.get(key: "tema") as! Int)

                   }
               }

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
  
    
    //MARK: TABLEVIEW
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dados.count;
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 56;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellIcon") as! CellIcon;
        cell.initJson(data: dados[indexPath.row]);
        return cell;
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if(indexPath.row == 0)
        {
            present(AppDelegate.STAGE.getSobre(), animated: true, completion: nil);
        }
        
        if(indexPath.row == 1)
        {
            present(AppDelegate.STAGE.getAjuda(), animated: true, completion: nil);
        }
        
        if(indexPath.row == 2)
        {
            addAlertOK(value: "Versão \(appVersion!)");
        }
    
    }
    
    
    @IBAction func onSelectTheme(_ sender: UISegmentedControl) {
      
       
        AppDelegate.APP.onSelectTema(index: sender.selectedSegmentIndex)

        
    }
    
    

 
    
    
   
    
    
}//END CLASS
